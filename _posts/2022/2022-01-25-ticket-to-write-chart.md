---
title: 'Ticket to Write - chart'
type: post
tags: [ game, roll and write ]
comment: true
date: 2022-01-25 07:00:00 +0100
mathjax: false
published: true
---

**TL;DR**

> I started drafting **Ticket to Write**.

I based the initial map on [Ticket to Ride London][], because it has six
colors only (plus the jolly) so it's perfect for mapping onto die faces.

![Ticket to Write 0.1]({{ '/assets/images/ticket-to-write-0.1.svg' | prepend: site.baseurl }})

Next step will probably be the tickets sheet... and the individual
player's score boards. Let's see!

Stay safe folks, we're still in the middle of it.

[Ticket to Ride London]: https://boardgamegeek.com/boardgame/276894/ticket-ride-london
