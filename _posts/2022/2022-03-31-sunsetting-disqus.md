---
title: Sunsetting Disqus in this blog
type: post
tags: [ blog, web ]
comment: true
date: 2022-03-31 07:00:00 +0200
mathjax: false
published: true
---

**TL;DR**

> I'm removing comments support via [Disqus][] in this blog from now on.

I've used [Disqus][] for my blog comments from about the beginning, for
no specific reason than to give some way to provide feedback. [Disqus][]
is interesting to this regard, because it's comments as a service.

But, of course, there's this:

![disqus trackers]({{ '/assets/images/disqus-trackers.png' | prepend: site.baseurl }})

So I recently started thinking about removing it. The amount of feedback
I receive is also very very low, so inflicting the trackers to the
casual reader was not even helping me in some *unethical* way.

I'll continue to post announcements of new posts in [Twitter][tw] and
[Octodon][octodon], so I guess that if someone really wants to comment
knows where to find me...

Stay safe folks!

[Disqus]: https://disqus.com/
[tw]: https://twitter.com/polettix
[octodon]: https://octodon.social/@polettix
