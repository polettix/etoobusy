---
title: Codeberg - signing up
type: post
tags: [ codeberg, git ]
series: Codeberg
comment: true
date: 2022-07-08 07:00:00 +0200
mathjax: false
published: true
---

**TL;DR**

> My experience signing up with [Codeberg][].

Signing up with [Codeberg][] *SHOULD* be a breeze, but it was not for me
so I thought it better to share the experience.

The bottom line is: I was not receiving the email to confirm my address
and my account. This meant that I was stuck in a limbo in which I could
login, but do almost nothing (basically, only request to re-send the
email for confirmation).

The bottom line was *probably* that I have an address in my own domain,
and for some reasons this made the confirmation email disappear in a
void somewhere. [This was confirmed a bit later][confirm].

If anybody hits this kind of roadblocks, I'd suggest to head over to the
[Contacts][] page and use one of the suggested ways of getting in touch.

For me the magic channel was the [Codeberg room][] in [Matrix][], but I
don't know if it dependend on the time or day I asked for help. Anyway
the people there were very patient and supportive, which is possibly a
much better start than the case that I didn't have the problem in the
first place.

Somebody probably touched the right knobs at a certain time, because I
received all the emails at once... and managed to be onboarded. Yay!

Stay safe folks!

[Codeberg]: https://codeberg.org/
[Contacts]: https://docs.codeberg.org/contact/
[Codeberg room]: https://matrix.to/#/#codeberg.org:matrix.org
[Matrix]: https://matrix.to/
[confirm]: https://mastodon.technology/@codeberg/108584356992478220
